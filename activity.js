db.fruitsCollection.insertMany([
		{
			"name": "Banana",
			"supplier": "Farmer Fruits Inc.",
			"stocks": 30,
			"price": 20,
			"onSale": true
		},
		{
			"name": "Mango",
			"supplier": "Mango Magic Inc.",
			"stocks": 50,
			"price": 70,
			"onSale": true
		},
		{
			"name": "Dragon Fruit",
			"supplier": "Farmer Fruits Inc.",
			"stocks": 10,
			"price": 60,
			"onSale": true
		},
		{
			"name": "Grapes",
			"supplier": "Fruity Co.",
			"stocks": 30,
			"price": 100,
			"onSale": true
		},
		{
			"name": "Apple",
			"supplier": "Apple Valley",
			"stocks": 0,
			"price": 20,
			"onSale": false
		},
		{
			"name": "Papaya",
			"supplier": "Fruity Co.",
			"stocks": 15,
			"price": 60,
			"onSale": true
		}
]);

// 2
db.fruitsCollection.aggregate([
		{
			$match: {"onSale": true}
		},
		{
			$count : "fruitsOnSale"
		}


	]);

// 3
db.fruitsCollection.aggregate([
		{
			$match: {"stocks": {$gte:20}}
		},
		{
			$count: "enoughStocks"
		}
	]);



// 4
db.fruitsCollection.aggregate([
	{
		$match: {"onSale": true}
	},
	{
		$group: {_id:"$supplier", avgPrice:{ $avg: "$price"} }
	}
	]);

// 5 
db.fruitsCollection.aggregate([
		{
			$group: {_id: "$supplier", maxPrice: { $max: "$price" } } // gets the maximum value of $amount field
		}
	]);

// 6
db.fruitsCollection.aggregate([
		{
			$group: {_id: "$supplier", minPrice: { $min: "$price" } } // gets the maximum value of $amount field
		}
	]);



